<?php
return array(
	//以下配置一般不需要修改
	'DEFAULT_CONTROLLER' => 'index',	//默認的控制器
	'DEFAULT_ACTION'	 => 'index',	//默認的行為
	'CONTENT_HOLDER'	=> '{__CONTENT__}',	//默認模板取代的名稱
	
	//數據庫
	'DB_HOST' 			=> 'localhost',
	'DB_PORT' 			=> 3306,
	'DB_NAME' 			=> 'kevindb',
	'DB_USER' 			=> 'root',
	'DB_PWD' 			=> '',
	'DB_DSN'			=> 'sqlite:model/kevindb.db', 	//sqlite所用的DSN，要先安裝 apt-get install  php7.0-sqlite3
);