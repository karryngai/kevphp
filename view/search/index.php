<style scoped>
.article{ 
	padding: 0 20px 20px 20px;
	margin-bottom: 25px;
}
.article .weather{ padding-left:20px; }
</style>
<?php
$item = $GLOBALS['data'];

echo '<div class="article">';
echo '<h2><a href="'.url('search','index', array('id'=>$item['id'])).'">'.$item['title'].'</a></h2>';
if(!empty($item['time'])){
	echo '<p>';
	echo '<span class="time"><i class="fa fa-calendar"></i> '.date('Y-n-d', $item['time']).'</span>';
	if(!empty($item['weather'])){
		echo '<span class="weather">'.$item['weather'].'</span>';
	}
	echo '</p>';
}
echo '<div>'.$item['article'].'</div>';
echo '</div>';
?>
<hr />
<p><a class="pure-button pure-button-primary" href="javascript:history.back()">返回上一頁</a></p>