<!DOCTYPE html>
<html>
<head>
<title>Kevin Blog</title>
<meta charset="utf-8" />
<link rel="stylesheet" href="public/css/pure-min.css" />
<!--[if lte IE 8]>
	<link rel="stylesheet" href="public/css/grids-responsive-old-ie-min.css" />
<![endif]-->
<!--[if gt IE 8]><!-->
	<link rel="stylesheet" href="public/css/grids-responsive-min.css" />
<!--<![endif]-->
<link rel="stylesheet" href="public/css/css/fontawesome-all.min.css" />
<style>
body{ padding-bottom:20px; }
h2 a, td a{ text-decoration:none; color:#000; }
h2 a:hover, td a:hover{ text-decoration:underline; color:#215B63; }
#menu{ font-size:20px; }
.l-box{ padding: 3px 20px; }
.article{ line-height:1.8em; }
</style>
</head>
<body>
<div class="pure-g">
    <div class="pure-u-1">
		<div class="pure-menu pure-menu-horizontal" id="menu">
			<a href="index.php" class="pure-menu-heading pure-menu-link">KEVIN Blog</a>
			<ul class="pure-menu-list">
			
				<?php if(CONTROLLER=='index'): //主頁 ?>
					<li class="pure-menu-item pure-menu-selected"><a href="<?php echo url('index', 'index'); ?>" class="pure-menu-link">首頁</a></li>
				<?php else: ?>
					<li class="pure-menu-item"><a href="<?php echo url('index', 'index'); ?>" class="pure-menu-link">首頁</a></li>
				<?php endif; ?>
				
				<?php if(CONTROLLER=='search' && get('name')=='心情'): //標簽含有心情 ?>
					<li class="pure-menu-item pure-menu-selected"><a href="<?php echo url('search', 'tag', array('name'=>'心情')); ?>" class="pure-menu-link">心情</a></li>
				<?php else:?>
					<li class="pure-menu-item"><a href="<?php echo url('search', 'tag', array('name'=>'心情')); ?>" class="pure-menu-link">心情</a></li>
				<?php endif; ?>
				
				<?php if(CONTROLLER=='search' && get('name')=='社會'): //標簽含有社會 ?>
					<li class="pure-menu-item pure-menu-selected"><a href="<?php echo url('search', 'tag', array('name'=>'社會')); ?>" class="pure-menu-link">社會</a></li>
				<?php else:?>
					<li class="pure-menu-item"><a href="<?php echo url('search', 'tag', array('name'=>'社會')); ?>" class="pure-menu-link">社會</a></li>
				<?php endif; ?>
				
				<?php if(islogin()): 		//文章管理，登入了才能使用
						if(CONTROLLER=='list'): ?>
							<li class="pure-menu-item pure-menu-selected"><a href="<?php echo url('list', 'index'); ?>" class="pure-menu-link">文章管理</a></li>
				<?php else: ?>
							<li class="pure-menu-item"><a href="<?php echo url('list', 'index'); ?>" class="pure-menu-link">文章管理</a></li>
				<?php endif; endif; ?>
				
				<?php if(!islogin()):		//用戶未登入，顯示登入字樣
						if(CONTROLLER=='login'): ?>
							<li class="pure-menu-item pure-menu-selected"><a href="<?php echo url('login', 'index'); ?>" class="pure-menu-link">登入</a></li>
				<?php else:?>
							<li class="pure-menu-item"><a href="<?php echo url('login', 'index'); ?>" class="pure-menu-link">登入</a></li>
				<?php endif; endif;?>

				<?php if(islogin()):		//用戶已登入，顯示登出字樣 ?>
					<li class="pure-menu-item"><a href="<?php echo url('logout', 'index'); ?>" class="pure-menu-link">登出</a></li>
				<?php endif;?>
				
			</ul>
		</div>
	</div>
    <div class="pure-u-1">
		<div class="l-box">{__CONTENT__}</div>
	</div>
</div>
</body>
</html>
