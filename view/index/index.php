<style scoped>
.article{ 
	padding: 0 20px 20px 20px;
	margin-bottom: 25px;
	border: 1px solid rgba(0,0,0,0.2); 
}
.article .weather{ padding-left:20px; }
</style>
<?php 
$data = $GLOBALS['data']; 
if(count($data)==0){
	echo '<p>對不起，找不到任何文章！</p>';
}else{
	foreach($data as $item){
		echo '<div class="article">';
		echo '<h2><a href="'.url('search','index', array('id'=>$item['id'])).'">'.$item['title'].'</a></h2>';
		if(!empty($item['time'])){
			echo '<p>';
			echo '<span class="time"><i class="fa fa-calendar"></i> '.date('Y-n-d', $item['time']).'</span>';
			if(!empty($item['weather'])){
				echo '<span class="weather">'.$item['weather'].'</span>';
			}
			echo '</p>';
		}
		echo '<div>'.cutstr($item['article'],300).'</div>';
		echo '</div>';
	}
}
