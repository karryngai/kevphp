<style scoped>
.kbox{ padding: 10px 20px; }
</style>
<div class="kbox">
	<form class="pure-form pure-form-aligned" action="<?php echo url('login', 'validate'); ?>" method="post">
		<fieldset>
			<legend>登入管理系統</legend>
			<div class="pure-control-group">
				<label for="name">用戶名</label>
				<input id="name" name="name" type="text" placeholder="用戶名">
			</div>
			<div class="pure-control-group">
				<label for="password">密碼</label>
				<input id="password" name="password" type="password" placeholder="密碼">
			</div>
			<div class="pure-controls">
				<button type="submit" class="pure-button pure-button-primary">登入</button>
			</div>
		</fieldset>
	</form>
</div>