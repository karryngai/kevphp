<?php if($GLOBALS['count']==0): ?>
	<p>目前沒有任何文章。</p>
<?php else: 
	$data = $GLOBALS['data']; ?>
<h3>文章列表</h3>
<style scoped>table th{text-align: center;}</style>
<table class="pure-table pure-table-striped">
    <thead>
        <tr><th>標題</th><th>時間</th><th>天氣</th><th>文章</th><th>標簽</th><th>修改</th></tr>
    </thead>
    <tbody>
		<?php foreach($data as $item): ?>
				<tr>
					<td><a href="<?php echo url('search','index',array('id'=>$item['id'])); ?>"><?php echo $item['title']; ?></a></td>
					<td><?php echo !empty($item['time']) ? date('Y-n-d',$item['time']) : '-'; ?></td>
					<td><?php echo !empty($item['weather']) ? $item['weather'] : '-'; ?></td>
					<td><?php echo cutstr($item['article'],50); ?></td>
					<td><?php echo !empty($item['tags']) ? $item['tags'] : '-'; ?></td>
					<td>
						<div class="pure-button-group" role="group">
							<a class="pure-button" href="<?php echo url('list','update', array('id'=>$item['id'])); ?>">
								<i class="fa fa-edit"></i>
							</a>
							<a class="pure-button" href="<?php echo url('list','delete', array('id'=>$item['id'])); ?>">
								<i class="fa fa-trash"></i>
							</a>
						</div>
					</td>
				</tr>
		<?php endforeach; ?>
    </tbody>
</table>
	
<?php endif; ?>

<p><a class="pure-button pure-button-primary" href="<?php echo url('list', 'add'); ?>">添加新文章</a></p>

