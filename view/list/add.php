<form class="pure-form pure-form-stacked" method="post" action="<?php echo url('list', 'add'); ?>">
    <fieldset>
        <legend>添加新文章</legend>

        <label for="title">標題</label>
        <input id="title" name="title" type="text" placeholder="Title" />
        <span class="pure-form-message">不要忽略標題</span>

        <label for="time">時間</label>
        <input id="time" name="time" type="date" placeholder="Time" value="<?php echo date('Y-m-d'); ?>" />

        <label for="weather">天氣</label>
        <input id="weather" name="weather" type="text" placeholder="Weather" />

        <label for="article">文章</label>
		<textarea id="article" name="article" style="width:100%" rows="30"></textarea>
		
		<label for="tags">標簽</label>
        <input id="tags" name="tags" type="text" placeholder="Tags" />

        <button type="submit" class="pure-button pure-button-primary">添加文章</button>
    </fieldset>
</form>