<?php $data = $GLOBALS['data']; ?>
<form class="pure-form pure-form-stacked" method="post" action="<?php echo url('list', 'update', array('id'=>$data['id'])); ?>">
    <fieldset>
        <legend>修改文章</legend>
		<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
		
        <label for="title">標題</label>
        <input id="title" name="title" type="text" placeholder="Title" value="<?php echo $data['title']; ?>" />
        <span class="pure-form-message">不要忽略標題</span>

        <label for="time">時間</label>
		<?php if(empty($data['time'])): ?>
			<input id="time" name="time" type="date" placeholder="Time" />
		<?php else: ?>
			<input id="time" name="time" type="date" placeholder="Time" value="<?php echo date('Y-m-d', $data['time']); ?>" />
		<?php endif; ?>

        <label for="weather">天氣</label>
        <input id="weather" name="weather" type="text" placeholder="Weather" value="<?php echo $data['weather']; ?>" />

        <label for="article">文章</label>
		<textarea id="article" name="article" style="width:100%" rows="30"><?php echo $data['article']; ?></textarea>

		<label for="tags">標簽</label>
        <input id="tags" name="tags" type="text" placeholder="Tags" value="<?php echo $data['tags']; ?>" />
		
		<a class="pure-button" href="<?php echo url('list','index'); ?>">回上一頁</a>
		<button type="reset" class="pure-button">重置</button>
		<button type="submit" class="pure-button pure-button-primary">修改文章</button>
    </fieldset>
</form>