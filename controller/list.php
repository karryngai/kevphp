<?php
//安全性處理，若嘗未登陸，就無法進行文章的編輯
if(!islogin()) exit;		

//顯示所有數據
if(ACTION=='index' && METHOD=='GET'){
	$pdo = db_connect();
	$count = db_count($pdo, 'blog');
	if($count>0){
		$data = db_query($pdo, 'select * from `blog` order by `time` desc');
	}
	display('base', 'list/index');
}
//添加文章的頁面
else if(ACTION=='add' && METHOD=='GET'){
	display('base', 'list/add');
}
//添加文章
else if(ACTION=='add' && METHOD=='POST'){
	require 'model/blog.php';		//插入數據時，需要處理數據合法性
	$pdo = db_connect();
	$result = db_exec($pdo, 'insert into blog(`title`,`time`,`weather`,`article`,`tags`) values(?,?,?,?,?)', 
						array($title, $time, $weather, $article, $tags));
	if($result){
		redirect(url('redirect','index',array('code'=>6)));
	}
}
//更新文章的頁面
else if(ACTION=='update' && METHOD=='GET'){
	$pdo = db_connect();
	$id = $_GET['id'];
	$data = db_query($pdo, 'select * from `blog` where id=? limit 1', array($id));
	$data = $data[0];
	display('base', 'list/update');		//顯示模板
}
//更新文章
else if(ACTION=='update' && METHOD=='POST'){
	require 'model/blog.php';
	$id = $_POST['id'];
	$pdo = db_connect();
	$result = db_exec($pdo, 'update blog set `title`=?,`time`=?,`weather`=?,`article`=?,`tags`=? where `id`=?', 
						array($title, $time, $weather, $article, $tags, $id));
	if($result){
		redirect(url('redirect','index',array('code'=>7)));
	}else{
		redirect(url('redirect','index',array('code'=>8)));
	}
}
//刪除文章的頁面
else if(ACTION=='delete' && METHOD=='GET'){
	$id = $_GET['id'];
	display('base', 'list/delete');		//顯示模板
}
//刪除文章
else if(ACTION=='delete' && METHOD=='POST'){
	$id = $_POST['id'];
	$pdo = db_connect();
	$result = db_exec($pdo, 'delete from blog where `id`=?', array($id));
	if($result){
		redirect(url('redirect','index',array('code'=>9)));
	}else{
		redirect(url('redirect','index',array('code'=>10)));
	}
}

