<?php
//驗證用戶名和密碼是否正確，若正確就登入
if(ACTION=="validate" && METHOD=='POST'){
	//獲取用戶數據
	$user = require('conf/user.php');
	$name = $_POST['name'];
	$password = $_POST['password'];
	
	//檢驗用名和密碼
	if(isset($user[$name]) && $password==$user[$name]){
		setlogin($name);		//代表已經登入了
		redirect(url('redirect', 'index', array('code'=>1)));		//重定向
	}else{
		setlogin(0);			//代表已經嘗未登入
		redirect(url('redirect', 'index', array('code'=>2)));		//重定向
	}
}else{
	display('base', 'login/index');			//顯示模板
}
