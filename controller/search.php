<?php
//獨立顯示某篇文章
if(ACTION=='index' && METHOD=='GET'){
	$pdo = db_connect();
	$data = db_query($pdo, 'select * from blog where id=? limit 1', array($_GET['id']));
	if(count($data)>0){
		$data = $data[0];
		display('base', 'search/index');
	}	
}
//顯示某個標簽的所有文章
else if(ACTION=='tag' && METHOD=='GET'){
	$pdo = db_connect();
	$name = urldecode($_GET['name']);
	$data = db_query($pdo, 'select * from blog where tags like ?', array('%'.$name.'%'));
	display('base', 'index/index');		//和主頁一樣，只是內容不同了
}