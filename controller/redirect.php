<?php
//對各種成功或失敗的行為，進行重定向
$messages = array(
	1 => array('msg'=>'你已成功登入系統，2秒後重定向到<a href="{__LINK__}">這裡</a>...', 'url'=>url('index')),
	2 => array('msg'=>'你的帳號或密碼存在問題無法登入，2秒後回到<a href="{__LINK__}">這裡</a>...', 'url'=>url('login')),
	3 => array('msg'=>'成功登出系統，2秒後回到<a href="{__LINK__}">這裡</a>...', 'url'=>url('index')),
	4 => array('msg'=>'標題不能為空，2秒後返回<a href="javascript:history.back()">上一頁</a>...', 'url'=>'back'),	//back是利用javascript返回上一頁
	5 => array('msg'=>'文章不能為空，2秒後返回<a href="javascript:history.back()">上一頁</a>...', 'url'=>'back'),
	6 => array('msg'=>'文章添加成功，2秒後重定向到<a href="{__LINK__}">這裡</a>...', 'url'=>url('list')),
	7 => array('msg'=>'文章修改成功，2秒後重定向到<a href="{__LINK__}">這裡</a>...', 'url'=>url('list')),
	8 => array('msg'=>'文章修改失敗，2秒後返回<a href="javascript:history.back()">上一頁</a>', 'url'=>'back'),
	9 => array('msg'=>'文章刪除成功，2秒後重定向到<a href="{__LINK__}">這裡</a>...', 'url'=>url('list')),
	10 => array('msg'=>'文章刪除失敗，2秒後重定向到<a href="{__LINK__}">這裡</a>...', 'url'=>url('list')),
);
$code = $_GET['code'];
$msg = $messages[$code]['msg'];
$url = $messages[$code]['url'];
$msg = str_replace('{__LINK__}', $url, $msg);	//把鏈結導入到信息中去

//顯示模板
display('redirect/index');
