<?php
session_start();

//記錄請求方法
define('METHOD', $_SERVER['REQUEST_METHOD']);

//載入配置文檔
$conf = require APP_PATH.'conf/config.php';
foreach($conf as $name=>$value){
	define($name, $value);
}

//記錄當前的控制器
if(isset($_GET['c'])){
	define('CONTROLLER', $_GET['c']);
}else{
	define('CONTROLLER', DEFAULT_CONTROLLER);
}

//記錄當前的行為
if(isset($_GET['a'])){
	define('ACTION', $_GET['a']);
}else{
	define('ACTION', DEFAULT_ACTION);
}

//載入函數庫
require APP_PATH.'library/function.php';
require APP_PATH.'library/login.php';

//載入控制器
require APP_PATH.'controller/'.CONTROLLER.'.php';

