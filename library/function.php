<?php
/**
 * 顯示視圖
 * @param string $base 佈局文件
 * @param array $array 需要取代佔位符的字符串陣列
 * @return string 佈局文件被取代後的內容
 */
function display($base, $array=null){
	ob_start();
	require APP_PATH."view/".$base.'.php';
	$base = ob_get_clean();

	if(!empty($array)){
		if(!is_array($array)) $array = array($array);
		foreach($array as $page){
			ob_start();
			require APP_PATH."view/".$page.'.php';
			$page = ob_get_clean();
			$base = str_replace(CONTENT_HOLDER, $page, $base);		//把佔位符逐一取代掉
		}
	}
	echo $base;	
}
/**
 * 生成鏈結
 * @param string $controller 指定的控制器，默認為'index'
 * @param string $action 指定的行為，默認為'index'
 * @param string $array 查詢字符串後面更多的參數陣列
 * @return string 生成的新鏈結
 */
function url($controller='index', $action='index', $array=null){
	$url = 'index.php?c='.$controller.'&a='.$action;
	if(is_array($array)){
		$url .= '&'.http_build_query($array);
	}
	return $url;
}
/**
 * GET請求的參數值
 * @param string $name 查詢字符串的名稱
 * @param string $default 若不存在時，所使用的默認值
 * @return string 獲取參數值
 */
function get($name, $default=''){
	return isset($_GET[$name]) ? $_GET[$name] : $default;
}
/**
 * 跳轉到某個鏈結
 * @param string $url 要跳轉的鏈結
 */
function redirect($url){
	header('Location: '.$url);
	exit;		//不加exit，就會執行後面的代碼
}

/**
 * 連結到數據庫
 * @return PDO 返回PDO對象
 */
function db_connect(){
	try{
		if(defined('DB_DSN')){
			$pdo = new PDO(DB_DSN, null, null, array(PDO::ATTR_PERSISTENT=>true));
		}else{
			$pdo = new PDO('mysql:host='.DB_HOST.';port='.DB_PORT.';dbname='.DB_NAME, 
							DB_USER, DB_PWD, 
							array(PDO::ATTR_PERSISTENT=>true));
		}
		return $pdo;
	}
	catch(PDOException $e){
		echo '連結失敗：'.$e->getMessage();
		exit;
	}
}
/**
 * 計算數據表有多少行
 * @param PDO $pdo PDO對象
 * @param string $table 數據表的名稱
 * @return int 得到行數
 */
function db_count($pdo, $table){
	$sth = $pdo->prepare("select count(*) from $table");
	$sth->execute();
	$row = $sth->fetch(PDO::FETCH_NUM);
	return $row[0];
}
/**
 * 執行SQL語句，用在插入和更新中
 * @param PDO $pdo PDO對象
 * @param string $sql SQL語句
 * @param array $val SQL語句中?的參數值
 * @return mixed 是否成功執行了
 */ 
function db_exec($pdo, $sql, $val=null){
	$sth = $pdo->prepare($sql);
	return $sth->execute($val);
}
/**
 * 執行SQL語句，用在select中
 * @param PDO $pdo PDO對象
 * @param string $sql SQL語句
 * @param array $val SQL語句中?的參數值
 * @return array 獲取的數據
 */
function db_query($pdo, $sql, $val=null){
	$sth = $pdo->prepare($sql);
	$sth->execute($val);
	return $sth->fetchAll(PDO::FETCH_ASSOC);
} 
/**
 * 縮短字符串，用於顯示
 */
function cutstr($str, $len=50){
	$str = strip_tags($str, $len);
	if(strlen($str)>$len){
		return mb_substr($str, 0, $len).'......'; 
	}else{
		return $str;
	}			
}
