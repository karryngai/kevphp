<?php
/**
 * 用戶是否登入了
 * @return bool 登入了就返回true，否則返回false
 */
function islogin(){
	return isset($_SESSION['login']) && !empty($_SESSION['login']);
}
/**
 * 在session中記錄信息
 */
function setlogin($val){
	$_SESSION['login'] = $val;	
}